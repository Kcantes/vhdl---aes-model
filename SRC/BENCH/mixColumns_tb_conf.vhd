--Quentin Chevalier 06/12/2019, testbench conf mixColumns

library LIB_RTL;

configuration mixColumns_tb_conf_1 of mixColumns_tb is
	for mixColumns_tb_arch
		for DUT : mixColumns
			use configuration lib_rtl.mixcolumns_conf;
		end for;
	end for;
end mixColumns_tb_conf_1;