--23/11/2019, Quentin Chevalier subByte entity, subByte Arch
library ieee;
use ieee.std_logic_1164.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

--entity
entity subByte is
  port (
    data_i : in type_state;
    data_o : out type_state
  );
end subByte;

--arch
architecture subByte_arch of subByte is
  component sBox
    port (
      data_i : in bit8; --bit8 : std_logic_vector of 8 bits
      data_o : out bit8
    );
  end component;

begin -- generate 16 sbox to fill a 4x4 matrix (type_state) via lookup table 
  Gen_sBox : for I in 0 to 15 generate
    sBox_n : sBox
    port map
    (
      --inputs map
      data_i => data_i(I/4)(I mod 4),
      data_o => data_o(I/4)(I mod 4)
    );
  end generate Gen_sBox;
end architecture; -- arch

--conf
configuration subByte_conf of subByte is
  for subByte_arch
    for Gen_sBox
      for sbox_n : sBox
        use entity Lib_rtl.sbox(sbox_arch);
      end for;
    end for;
  end for;
end configuration subByte_conf;