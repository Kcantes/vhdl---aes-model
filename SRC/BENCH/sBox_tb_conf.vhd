--Quentin Chevalier 23/11/2019, testbench conf simple sBox
library LIB_RTL;

configuration sBox_tb_conf_1 of sBox_tb is
	for sBox_tb_arch
		for DUT : sBox
			use entity lib_rtl.sBox(sBox_arch);
		end for;
	end for;
end sBox_tb_conf_1;