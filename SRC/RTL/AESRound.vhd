--File: AESRound.vhd
--Project: AES encryption in VHDL
--File Created: Saturday, 7th December 2019 7:02:51 pm
--Author: Kcantes (chevaliq2@gmail.com)
-------
--Last Modified: Saturday, 7th December 2019 7:03:15 pm
--Modified By: Kcantes (chevaliq2@gmail.com)
-------
--Copyright 2019 - 2019, Quentin Chevalier
library ieee;
use ieee.std_logic_1164.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

--entity
entity AESRound is
    port (
        clock_i : in std_logic;
        currentkey_i : in bit128;
        enable_mixcolumns_i : in std_logic;
        enable_Roundcomputing_i : in std_logic;
        resetb_i : in std_logic;
        text_i : in bit128;
        data_o : out bit128
    );
end AESRound;

--arch
architecture AESRound_arch of AESRound is

    component addroundkey is
        port (
            data_i : in type_state;--4x4 matrix
            key_i : in type_state;
            data_o : out type_state
        );
    end component;

    component subbyte is
        port (
            data_i : in type_state;
            data_o : out type_state
        );
    end component;

    component shiftrows is
        port (
            data_i : in type_state;
            data_o : out type_state
        );
    end component;

    component mixcolumns is
        port (
            data_i : in type_state;
            enable : in std_logic;
            data_o : out type_state);
    end component;

    component registre is
        port (
            clock_i : in std_logic;
            resetb_i : in std_logic;
            d_i : in type_state;
            q_o : out type_state
        );
    end component;
    -- dans l'ordre signaux : conversion du texte clair en type_state, input de addroundkey, conversion de la clé en type_state
    -- signal de sortie du système
    signal text_state_s, inputARK_s, currentkeystate_s, outputARK_s, data_s : type_state;
    -- entree du registre, sortie du registre
    signal reg_i_s, reg_o_s : type_state;
    -- sortie de subbox, shiftrows et mixcolumns
    signal subbox_o_s, shiftrows_o_s, mixcol_o_s : type_state;

begin

    --conversion
    G1 : for col in 0 to 3 generate
        G2 : for row in 0 to 3 generate
            text_state_s(row)(col) <= text_i(127 - 32 * col - 8 * row downto 120 - 32 * col - 8 * row);
            currentkeystate_s(row)(col) <= currentkey_i(127 - 32 * col - 8 * row downto 120 - 32 * col - 8 * row);
        end generate;
    end generate;

    --entree de addroundkey
    inputARK_s <= text_state_s when enable_Roundcomputing_i = '0' else
        mixcol_o_s;

    ARK : addroundkey port map(data_i => inputARK_s, key_i => currentkeystate_s, data_o => outputARK_s);
    reg_i_s <= outputARK_s;
    --sauvegarde de ark_o
    reg : registre port map(clock_i => clock_i, resetb_i => resetb_i, d_i => reg_i_s, q_o => reg_o_s);
    data_s <= reg_o_s;-- pas utile mais mieux pour ma compréhension
    --calculs sur le resultat
    SB : subbyte port map(data_i => data_s, data_o => subbox_o_s);
    SR : shiftrows port map(data_i => subbox_o_s, data_o => shiftrows_o_s);
    MC : mixColumns port map(data_i => shiftrows_o_s, enable => enable_mixcolumns_i, data_o => mixcol_o_s);

    -- conversion finale
    G3 : for col in 0 to 3 generate
        G4 : for row in 0 to 3 generate
            data_o(127 - 32 * col - 8 * row downto 120 - 32 * col - 8 * row) <= data_s(row)(col);
        end generate;
    end generate;

end architecture; -- arch

--configuration
configuration AESRound_conf of AESRound is
    for AESRound_arch
        for ARK : addroundkey
            use entity Lib_rtl.addroundkey(addroundkey_arch);
        end for;
        for SB : subbyte
            use configuration Lib_rtl.subbyte_conf;
        end for;
        for SR : shiftrows
            use entity Lib_rtl.shiftrows(shiftrows_arch);
        end for;
        for MC : mixcolumns
            use configuration Lib_rtl.mixcolumns_conf;
        end for;
        for reg : registre
            use entity Lib_rtl.registre(registre_arch);
        end for;
    end for;
end configuration AESRound_conf;