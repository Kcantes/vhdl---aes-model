--27/11/2019, Quentin Chevalier shiftrows entity, shiftrows Arch
library ieee;
use ieee.std_logic_1164.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

--entity
entity shiftRows is
  port (
    data_i : in type_state;
    data_o : out type_state
  );
end shiftRows;

--arch
architecture shiftRows_arch of shiftRows is
begin
  Gen_1 : for I in 0 to 3 generate
    Gen_2 : for J in 0 to 3 generate
      data_o(I)(J) <= data_i(I)(((J + I) mod 4));--décalage de la ligne I par le numéro de ligne I (commence à 0)
    end generate;
  end generate;
end architecture; -- arch