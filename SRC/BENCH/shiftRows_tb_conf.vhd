--Quentin Chevalier 27/11/2019, testbench conf shiftRows
library LIB_RTL;

configuration shiftRows_tb_conf_1 of shiftRows_tb is
	for shiftRows_tb_arch
		for DUT : shiftRows
			use entity LIB_RTL.shiftRows(shiftRows_arch);
		end for;
	end for;
end shiftRows_tb_conf_1;