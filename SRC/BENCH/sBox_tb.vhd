--Quentin Chevalier 23/11/2019, testbench simple sBox
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

entity sBox_tb is
end sBox_tb;

architecture sBox_tb_arch of sBox_tb is
    component sBox
        port (
            data_i : in bit8;
            data_o : out bit8
        );
    end component;
    signal data_i_s, data_o_s : bit8;
begin
    DUT : sBox
    port map(
        data_i => data_i_s,
        data_o => data_o_s
    );
    data_i_s <= X"00", X"01" after 50 ns;--simple signal

end architecture sBox_tb_arch; -- arch