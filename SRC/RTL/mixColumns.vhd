--Quentin Chevalier 06/12/2019, mixColumns
library ieee;
use ieee.std_logic_1164.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

--entity
entity mixColumns is
  port (
    data_i : in type_state;
    enable : in std_logic;
    data_o : out type_state);

end entity mixColumns;

--arch
architecture mixColumns_arch of mixColumns is
  component mixColumn
    port (
      data_i : in column_state;
      data_o : out column_state
    );
  end component;

  signal data_o_s : type_state;

begin

  G1 : for J in 0 to 3 generate
    mixColumn_n : mixColumn
    port map(
      -- input
      data_i(0) => data_i(0)(J),
      data_i(1) => data_i(1)(J),
      data_i(2) => data_i(2)(J),
      data_i(3) => data_i(3)(J),
      -- output
      data_o(0) => data_o_s(0)(J),
      data_o(1) => data_o_s(1)(J),
      data_o(2) => data_o_s(2)(J),
      data_o(3) => data_o_s(3)(J)
    );
  end generate G1;
  data_o <= data_i when enable = '0'
    else
    data_o_s;-- is mixcolumns enable?

end architecture;

--conf
configuration mixcolumns_conf of mixColumns is
  for mixColumns_arch
    for G1
      for mixColumn_n : mixColumn
        use entity lib_rtl.mixcolumn(mixcolumn_arch);
      end for;
    end for;
  end for;
end configuration mixcolumns_conf;