--Quentin Chevalier 06/12/2019, testbench mixColumn
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

entity mixColumn_tb is
end mixColumn_tb;

architecture mixColumn_tb_arch of mixColumn_tb is
    component mixColumn
        port (
            data_i : in column_state;
            data_o : out column_state
        );
    end component;
    signal data_i_s, data_o_s : column_state;
begin
    DUT : mixColumn
    port map(
        data_i => data_i_s,
        data_o => data_o_s
    );
    data_i_s(0) <= X"16";--test signal, right result : 29 8E 6D E0
    data_i_s(1) <= X"91";
    data_i_s(2) <= X"06";
    data_i_s(3) <= X"ab";
end architecture mixColumn_tb_arch; -- arch