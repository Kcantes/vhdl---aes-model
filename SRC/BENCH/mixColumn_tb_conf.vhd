--Quentin Chevalier 06/12/2019, testbench conf simple mixColumn
library LIB_RTL;

configuration mixColumn_tb_conf_1 of mixColumn_tb is
	for mixColumn_tb_arch
		for DUT : mixColumn
			use entity lib_rtl.mixColumn(mixColumn_arch);
		end for;
	end for;
end mixColumn_tb_conf_1;