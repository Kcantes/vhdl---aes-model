--Quentin Chevalier, 19/12/2019, aes testbench conf
library LIB_RTL;

configuration AES_tb_conf of AES_tb is
    for AES_tb_arch
        for dut : aes
            use configuration lib_rtl.aes_conf;
        end for;
    end for;
end configuration AES_tb_conf;