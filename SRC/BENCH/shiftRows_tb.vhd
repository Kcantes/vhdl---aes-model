--Quentin Chevalier 27/11/2019, testbench shiftrows
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library LIB_AES;
use LIB_AES.crypt_pack.all;
library LIB_RTL;

entity shiftRows_tb is
end shiftRows_tb;

architecture shiftRows_tb_arch of shiftRows_tb is
    component shiftRows
        port (
            data_i : in type_state;
            data_o : out type_state
        );
    end component;
    signal data_i_s, data_o_s : type_state;
begin
    DUT : shiftRows
    port map(
        data_i => data_i_s,
        data_o => data_o_s
    );
    data_i_s(0)(0) <= X"02", X"01" after 50 ns;--simple signal
    data_i_s(0)(1) <= X"03", X"04" after 50 ns;--simple signal
    data_i_s(0)(2) <= X"05", X"06" after 50 ns;--simple signal
    data_i_s(0)(3) <= X"00", X"00" after 50 ns;--simple signal
    data_i_s(1)(0) <= X"02", X"01" after 50 ns;--simple signal
    data_i_s(1)(1) <= X"03", X"04" after 50 ns;--simple signal
    data_i_s(1)(2) <= X"05", X"06" after 50 ns;--simple signal
    data_i_s(1)(3) <= X"00", X"00" after 50 ns;--simple signal
    data_i_s(2)(0) <= X"02", X"01" after 50 ns;--simple signal
    data_i_s(2)(1) <= X"03", X"04" after 50 ns;--simple signal
    data_i_s(2)(2) <= X"05", X"06" after 50 ns;--simple signal
    data_i_s(2)(3) <= X"00", X"00" after 50 ns;--simple signal
    data_i_s(3)(0) <= X"02", X"01" after 50 ns;--simple signal
    data_i_s(3)(1) <= X"03", X"04" after 50 ns;--simple signal
    data_i_s(3)(2) <= X"05", X"06" after 50 ns;--simple signal
    data_i_s(3)(3) <= X"00", X"00" after 50 ns;--simple signal
end architecture shiftRows_tb_arch; -- arch