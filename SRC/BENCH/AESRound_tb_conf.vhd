--Quentin Chevalier 27/11/2019, testbench conf AESRound
library LIB_RTL;

configuration AESRound_tb_conf_1 of AESRound_tb is
	for AESRound_tb_arch
		for DUT : AESRound
			use entity lib_rtl.AESRound(AESRound_arch);
		end for;
	end for;
end AESRound_tb_conf_1;